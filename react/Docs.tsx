import React, { createElement as h } from "https://esm.sh/react";
import About from "./docs/About.tsx";
import Download from "./docs/Download.tsx";
import Examples from "./docs/Examples.tsx";
import HowItWorks from "./docs/HowItWorks.tsx";
import MeaningsOfColors from "./docs/MeaningsOfColors.tsx";
import Stats from "./docs/Stats.tsx";
import Story from "./docs/Story.tsx";
import Topbar from "./docs/Topbar.tsx";

export default function App() {
  return (
    <div id="docs">
      <Topbar />
      <Story />
      <Examples />
      <Download />
      <Examples />
      <HowItWorks />
      <MeaningsOfColors />
      <Stats />
      <About />
    </div>
  );
}
