import React, { createElement as h } from "https://esm.sh/react";

export default function App() {
  return (
    <div class="doc" id="topbar">
      <a id="a-meaning-of-colors" href="#">Meanings of colors</a>
      | <a id="a-stats" href="#">Stats</a>
    </div>
  );
}
