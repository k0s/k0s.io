import React, { createElement as h } from "https://esm.sh/react";

export default function App() {
  return (
    <div class="doc" id="meaning-of-colors">
      <h1>Meanings of colors</h1>
      <fieldset>
        <legend>Legend</legend>
        <div class="foobar">
          <div class="foo secret"></div>
          <label class="bar" for="mothman">#5EC2E7 Basic Auth</label>
        </div>
        <div class="foobar">
          <div class="foo badass"></div>
          <label class="bar" for="sasquatch">#BADA55 No Auth</label>
        </div>
      </fieldset>
    </div>
  );
}
