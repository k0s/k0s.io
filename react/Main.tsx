import React, { createElement as h } from "https://esm.sh/react";
import Docs from "./Docs.tsx";

export default function App() {
  return (
    <div id="main">
      <div id="controller">
        <input id="api" value="https://libredot.com" />
        <button id="list">List</button>
        <button id="watch">Watch</button>
      </div>
      <div id="view">
        <Docs />
        <div id="agents"></div>
      </div>
    </div>
  );
}

/*
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-137403717-1');
  </script>
*/
