#!/usr/bin/env -S deno run -A --no-check

import React, { createElement as h } from "https://esm.sh/react";
import ReactDOMServer from "https://esm.sh/react-dom/server";
import App from "./Index.tsx";

function render() {
  // const html = ReactDOMServer.renderToString(<App />);
  const html = ReactDOMServer.renderToStaticMarkup(<App />);
  const body = `<!DOCTYPE html>${html}`;
  return body;
}

console.log(render());
