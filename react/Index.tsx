import React, { createElement as h } from "https://esm.sh/react";
import Main from "./Main.tsx";

export default function App() {
  return (
    <html>
      <head>
        <title>
          k0s | aggregate your personal infrastructure
        </title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link href="reset.css" rel="stylesheet" type="text/css" />
        <link href="style.css" rel="stylesheet" type="text/css" />
      </head>
      <body>
        <Main />
        <script
          async
          src="https://www.googletagmanager.com/gtag/js?id=UA-137403717-1"
        >
        </script>
        <script src="script.js"></script>
        <script src="dist/bundle.js"></script>
      </body>
    </html>
  );
}

/*
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-137403717-1');
  </script>
*/
